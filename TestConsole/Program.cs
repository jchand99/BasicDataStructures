/*
Author: Jacob Chandler
File: Program.cs
Version: 1.0.1
Description: This file is used to test functionality in a regular c# project.
Date of Comment: 06:01:2018
 */

using System;
using System.Diagnostics;
using DataStructures.Dictionaries;
using DataStructures.Iterator;
using DataStructures.Lists;
using DataStructures.Queues;
using DataStructures.Stacks;
using DataStructures.Trees;

namespace TestConsole {
    class Program {
        private static Stopwatch sw = new Stopwatch();
        private static SortedArrayList<string> SAList;
        private static LinkedList<string> LList;
        private static SortedLinkedList<string> SLList;
        private static Random r = new Random();

        public static void Main(string[] args) {
            int[] elements = new int[] { 10, 40, 30, 70, 80, 75, 43, 1, 12, 32 };

            //Test
            IBinaryTree<int> binaryTree = new BinaryTree<int>();
            for(int i = 0; i < elements.Length; i++) {
                binaryTree.Add(elements[i]);
            }
        }
    }
}